## Introduction
This is the Verilog implementation for FPGA-based video surveillance and motion detection. The development environment is the Altera DE2 developing board.

Thanks to the team member Zhi Jiang, Zhan Zhao and Yanyi Han, great work!!!



## Abstract
```
This paper presents a video image acquisition and processing technology based on FPGA, and we give a detailed discussion on the four main components of video systems,
which include the video image acquisition, storage, processing algorithms and video display monitor output.
The Verilog language is mainly used for logic design with the use of hardware and software co-design techniques. 
Experimental results show that the system leverages the parallel nature of the FPGA device and significantly improves the image processing speed. 
In a static context, it can real-time and accurately detect and track the moving target. 
```

## Code
The code are based on Verilog.

**DE2_LCM_CCD.v** : the main executing file

**motion_detect.v** : detect the moving objects

**CCD_Capture.v** : capture the video flow

**RAW2RGB.v** : convert original data to RGB format

......


## Copyright

Copyright (c) Bohan Zhuang. 2012

** This code is for non-commercial purposes only. For commerical purposes,
please contact Bohan Zhuang <bohan.zhuang@adelaide.edu.au> **

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

