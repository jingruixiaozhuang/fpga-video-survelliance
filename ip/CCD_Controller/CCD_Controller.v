module CCD_Controller (
  // Avalon clock interface siganals
  csi_clockreset_clk,
  csi_clockreset_reset_n,
  // Signals for Avalon-MM slave port
  avs_s1_address,
  avs_s1_chipselect,
  avs_s1_write,
  avs_s1_writedata,
  // export
  avs_s1_export_o_key,
  avs_s1_export_o_sw
);

input         csi_clockreset_clk;
input         csi_clockreset_reset_n;

input         avs_s1_address;
input         avs_s1_chipselect;
input         avs_s1_write;
input  [17:0] avs_s1_writedata;

output [2:0]  avs_s1_export_o_key;
output [17:0] avs_s1_export_o_sw;

reg    [2:0]  r_key;
reg    [17:0] r_sw;

// write to export
always@(posedge csi_clockreset_clk or negedge csi_clockreset_reset_n)
begin
  if (!csi_clockreset_reset_n)
  begin
    r_key <= 4'b1111;
    r_sw  <= 18'b00_0000_0000_0000_0000;
  end
  else
  begin
    if (avs_s1_address == 1'b0 && avs_s1_chipselect && avs_s1_write)
    begin
      r_key <= avs_s1_writedata[2-:3];
      r_sw  <= r_sw;
    end
    else if (avs_s1_address == 1'b1 && avs_s1_chipselect && avs_s1_write)
    begin
      r_key <= r_key;
      r_sw  <= avs_s1_writedata;
    end
  end
end

assign avs_s1_export_o_key = r_key;
assign avs_s1_export_o_sw  = r_sw;

endmodule