#include "CCD_Controller_API.h"
#include "system.h"
#include "CCD_Controller_regs.h"

unsigned char read_pio_key() {
  return IORD_CCD_CONTROLLER_PIO_KEY(KEY_PIO_BASE);
}

unsigned int read_pio_sw() {
  return IORD_CCD_CONTROLLER_PIO_SW(SW_PIO_BASE);
}

void write_ccd_key(unsigned char key) {
  IOWR_CCD_CONTROLLER_CCD_KEY(CCD_CONTROLLER_INST_BASE, key);
}

void write_ccd_sw(unsigned int sw) {
  IOWR_CCD_CONTROLLER_CCD_SW(CCD_CONTROLLER_INST_BASE, sw);
}
