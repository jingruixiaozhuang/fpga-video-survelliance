#ifndef  CCD_CONTROLLER_API_H
#define  CCD_CONTROLLER_API_H

unsigned char read_pio_key();
unsigned int read_pio_sw();
void write_ccd_key(unsigned char key);
void write_ccd_sw(unsigned int sw);

#endif
