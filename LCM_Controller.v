module	LCM_Controller(	//	Host Side
						iRed,
						iGreen,
						iBlue,
						oCoord_X,
						oCoord_Y,
						//	LCM Side
						LCM_DATA,
						LCM_VSYNC,
						LCM_HSYNC,
						LCM_DCLK,
						LCM_GRST,
						LCM_SHDB,
						//	Control Signals
						oDATA_REQ,
						iCLK,
						iRST_N	);
//	Host Side
input	[7:0]	iRed;
input	[7:0]	iGreen;
input	[7:0]	iBlue;
output reg [9:0] oCoord_X;
output reg [9:0] oCoord_Y;
//	LCM Side
output	[7:0]	LCM_DATA;
output			LCM_VSYNC;
output			LCM_HSYNC;
output			LCM_DCLK;
output			LCM_GRST;
output			LCM_SHDB;
//	Control Signals
output			oDATA_REQ;
input			iCLK;
input			iRST_N;
//	Internal Register and Wire
reg		[10:0]	H_Cont;
reg		[10:0]	V_Cont;
reg				mVGA_H_SYNC;
reg				mVGA_V_SYNC;
reg				mDATA_REQ;
reg		[1:0]	MOD_3;
reg		[1:0]	Pre_MOD_3;

//	Horizontal Parameter	( Pixel )
parameter	H_SYNC_CYC	=	1;
parameter	H_SYNC_BACK	=	151;
parameter	H_SYNC_ACT	=	960;
parameter	H_SYNC_FRONT=	59;
parameter	H_SYNC_TOTAL=	1171;
//	Virtical Parameter		( Line )
parameter	V_SYNC_CYC	=	1;
parameter	V_SYNC_BACK	=	13;
parameter	V_SYNC_ACT	=	240;
parameter	V_SYNC_FRONT=	8;
parameter	V_SYNC_TOTAL=	262;

assign	LCM_SHDB	=	1'b1;
assign	LCM_GRST	=	1'b1;
assign	LCM_DCLK	=	~iCLK;
assign	LCM_VSYNC	=	mVGA_V_SYNC;
assign	LCM_HSYNC	=	mVGA_H_SYNC;
assign	LCM_DATA	=	(MOD_3==2'h00)	?	iRed	:
						(MOD_3==2'h01)	?	iGreen	:
											iBlue	;
assign	oDATA_REQ	=	mDATA_REQ;

always@(posedge iCLK or negedge iRST_N)
begin
	if(!iRST_N)
	begin
		MOD_3		<=	2'b00;
		Pre_MOD_3	<=	2'b00;	
		mDATA_REQ	<=	1'b0;
		oCoord_X    <=  0;
		oCoord_Y    <=  0;
	end
	else
	begin
		Pre_MOD_3	<=	MOD_3;
		if(	H_Cont>H_SYNC_BACK-2 && H_Cont<(H_SYNC_TOTAL-H_SYNC_FRONT-2) &&
			V_Cont>V_SYNC_BACK+1 && V_Cont<(V_SYNC_TOTAL-V_SYNC_FRONT+1))
		begin
			if(MOD_3<2'b10)
			MOD_3	<=	MOD_3+1'b1;
			else
			MOD_3	<=	2'b00;
			if( (MOD_3==2'b01) && (Pre_MOD_3==2'b00) )
			begin
			mDATA_REQ	<=	1'b1;
			oCoord_X    <=  (H_Cont-(H_SYNC_CYC+H_SYNC_BACK-1))/3;
			oCoord_Y    <=  V_Cont-(V_SYNC_CYC+V_SYNC_BACK+2);
			end
			else
			mDATA_REQ	<=	1'b0;
		end
		else
		begin
			MOD_3	<=	2'b00;
			mDATA_REQ<=	1'b0;
		end
	end
end

//	H_Sync Generator, Ref. 18.42 MHz Clock
always@(posedge iCLK or negedge iRST_N)
begin
	if(!iRST_N)
	begin
		H_Cont		<=	0;
		mVGA_H_SYNC	<=	0;
	end
	else
	begin
		//	H_Sync Counter
		if( H_Cont < H_SYNC_TOTAL )
		H_Cont	<=	H_Cont+1;
		else
		H_Cont	<=	0;
		//	H_Sync Generator
		if( H_Cont < H_SYNC_CYC )
		mVGA_H_SYNC	<=	0;
		else
		mVGA_H_SYNC	<=	1;
	end
end

//	V_Sync Generator, Ref. H_Sync
always@(posedge iCLK or negedge iRST_N)
begin
	if(!iRST_N)
	begin
		V_Cont		<=	0;
		mVGA_V_SYNC	<=	0;
	end
	else
	begin
		//	When H_Sync Re-start
		if(H_Cont==0)
		begin
			//	V_Sync Counter
			if( V_Cont < V_SYNC_TOTAL )
			V_Cont	<=	V_Cont+1;
			else
			V_Cont	<=	0;
			//	V_Sync Generator
			if(	V_Cont < V_SYNC_CYC )
			mVGA_V_SYNC	<=	0;
			else
			mVGA_V_SYNC	<=	1;
		end
	end
end

endmodule