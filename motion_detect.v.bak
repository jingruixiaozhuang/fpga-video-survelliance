module motion_detect(
		//detect Side
		iclk,
		iRead_DATA1,
		iRead_DATA2,
		iRead,
		iclken,
		oData_R,
		oData_G,
		oData_B,
		//speaker side
		iAudio_clk,
		oSpeaker_cont,
		oSpeaker);

input			iclk;
input			iclken;
input			iAudio_clk;
input	[15:0]	iRead_DATA1;
input	[15:0]	iRead_DATA2;
input			iRead;

output	[9:0]	oData_R;
output	[9:0]	oData_G;
output	[9:0]	oData_B;
output	[21:0]	oSpeaker_cont;
output			oSpeaker;

//======================	motion detect	======================//
wire	[10:0]	mTap_0;
reg		[10:0]	mTap_1,mTap_2,mTap_3,
				mTap_4,mTap_5,mTap_6,
				mTap_7,mTap_8,mTap_9,mTap_10;
wire	[10:0]	rTap_0;
reg		[10:0]	rTap_1,rTap_2,rTap_3,
				rTap_4,rTap_5,rTap_6,
				rTap_7,rTap_8,rTap_9,rTap_10;
wire	[10:0]	sTap_0;
reg		[10:0]	sTap_1,sTap_2,sTap_3,
				sTap_4,sTap_5,sTap_6,
				sTap_7,sTap_8,sTap_9,sTap_10;
reg				X,Y,Z;
reg				F1,F2;
reg		[5:0]	Read_d;

always@(posedge iclk)
begin
	//---------------	binary	-------------------//	
	F1	<=	(	iRead_DATA1[14:10] + iRead_DATA1[9:5] + iRead_DATA1[4:0] )	>48;
	F2	<=	(	iRead_DATA2[14:10] + iRead_DATA2[9:5] + iRead_DATA2[4:0] )	>48;	
	//---------------------------------------------//
	mTap_1	<=	mTap_0;
	mTap_2	<=	mTap_1;
	mTap_3	<=	mTap_2;
	mTap_4	<=	mTap_3;
	mTap_5	<=	mTap_4;
	mTap_6	<=	mTap_5;
	mTap_7	<=	mTap_6;
	mTap_8	<=	mTap_7;
	mTap_9	<=	mTap_8;
	mTap_10	<=	mTap_9;
	//---------------	erode	-------------------//
	X		<=	(&mTap_0) & (&mTap_1) & (&mTap_2) &
				(&mTap_3) & (&mTap_4) & (&mTap_5) &
				(&mTap_6) & (&mTap_7) & (&mTap_8) &
				(&mTap_9) & (&mTap_10);
	//---------------------------------------------//
	rTap_1	<=	rTap_0;
	rTap_2	<=	rTap_1;
	rTap_3	<=	rTap_2;
	rTap_4	<=	rTap_3;
	rTap_5	<=	rTap_4;
	rTap_6	<=	rTap_5;
	rTap_7	<=	rTap_6;
	rTap_8	<=	rTap_7;
	rTap_9	<=	rTap_8;
	rTap_10	<=	rTap_9;
	//---------------	dilate	-------------------//
	Y		<=	(|rTap_0) | (|rTap_1) | (|rTap_2) |
				(|rTap_3) | (|rTap_4) | (|rTap_5) |
				(|rTap_6) | (|rTap_7) | (|rTap_8) |
				(|rTap_9) | (|rTap_10);
	//---------------------------------------------//
	sTap_1	<=	sTap_0;
	sTap_2	<=	sTap_1;
	sTap_3	<=	sTap_2;
	sTap_4	<=	sTap_3;
	sTap_5	<=	sTap_4;
	sTap_6	<=	sTap_5;
	sTap_7	<=	sTap_6;
	sTap_8	<=	sTap_7;
	sTap_9	<=	sTap_8;
	sTap_10	<=	sTap_9;
	//---------------	erode	-------------------//
	Z		<=	(&sTap_0) & (&sTap_1) & (&sTap_2) &
				(&sTap_3) & (&sTap_4) & (&sTap_5) &
				(&sTap_6) & (&sTap_7) & (&sTap_8) &
				(&sTap_9) & (&sTap_10);
	//---------------------------------------------//
	Read_d	<=	{Read_d[4:0],iRead};
end
//---------------	detect method 1	-------------------//
Tap_1 	u99	(	.clken(iRead),
				.clock(iclk),
				.shiftin(	(iRead_DATA1[14:10] ^ iRead_DATA2[14:10]) | 
							(iRead_DATA1[9:5] ^ iRead_DATA2[9:5]) |
							(iRead_DATA1[4:0] ^ iRead_DATA2[4:0]) ),
				.taps(mTap_0));
		
Tap_1 	u98	(	.clken(Read_d[5]),
				.clock(iclk),
				.shiftin(X),
				.taps(rTap_0));
//---------------	detect method 2	-------------------//
Tap_1 	u97	(	.clken(Read_d[0]),
				.clock(iclk),
				.shiftin(F1^F2),
				.taps(sTap_0));
//==================================================================//
assign	oData_R	=	clken ?  ( (Y|Z)	?	1023	:	{iRead_DATA1[14:10],5'h00}	)
							:{iRead_DATA1[14:10],5'h00};
assign	oData_G	=	{iRead_DATA1[9:5],5'h00};
assign	oData_B	=	{iRead_DATA1[4:0],5'h00};

//======================	Speaker Control		====================//
reg			SP;
reg	[21:0]	SP_cont;
reg	[23:0]	DLY_cont;

assign	oSpeaker_cont	=	SP_cont;
assign	oSpeaker		=	SP;

always@(posedge iAudio_clk)
begin
	SP_cont	<=	SP_cont+1'b1;
	if( Y|Z && clken)				//	if detected => turn on speaker
	DLY_cont	<=	0;
	else
	begin
		if(DLY_cont<24'hffffff)		//	20 * 2^24 ns
		begin
			DLY_cont	<=	DLY_cont+1;
			SP			<=	1;
		end
		else
		SP			<=	0;		
	end
end
//==================================================================//

endmodule
		