/* system.h
 *
 * Machine generated for a CPU named "cpu" as defined in:
 * d:\altera\72\quartus\workspace\DE2_LCM_CCD_detect_b\software\Detect_syslib\..\..\system_0.ptf
 *
 * Generated: 2011-04-28 19:55:24.574
 *
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/*

DO NOT MODIFY THIS FILE

   Changing this file will have subtle consequences
   which will almost certainly lead to a nonfunctioning
   system. If you do modify this file, be aware that your
   changes will be overwritten and lost when this file
   is generated again.

DO NOT MODIFY THIS FILE

*/

/******************************************************************************
*                                                                             *
* License Agreement                                                           *
*                                                                             *
* Copyright (c) 2003 Altera Corporation, San Jose, California, USA.           *
* All rights reserved.                                                        *
*                                                                             *
* Permission is hereby granted, free of charge, to any person obtaining a     *
* copy of this software and associated documentation files (the "Software"),  *
* to deal in the Software without restriction, including without limitation   *
* the rights to use, copy, modify, merge, publish, distribute, sublicense,    *
* and/or sell copies of the Software, and to permit persons to whom the       *
* Software is furnished to do so, subject to the following conditions:        *
*                                                                             *
* The above copyright notice and this permission notice shall be included in  *
* all copies or substantial portions of the Software.                         *
*                                                                             *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER      *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     *
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         *
* DEALINGS IN THE SOFTWARE.                                                   *
*                                                                             *
* This agreement shall be governed in all respects by the laws of the State   *
* of California and by the laws of the United States of America.              *
*                                                                             *
******************************************************************************/

/*
 * system configuration
 *
 */

#define ALT_SYSTEM_NAME "system_0"
#define ALT_CPU_NAME "cpu"
#define ALT_CPU_ARCHITECTURE "altera_nios2"
#define ALT_DEVICE_FAMILY "CYCLONEII"
#define ALT_STDIN "/dev/jtag_uart"
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN_BASE 0x00901070
#define ALT_STDIN_DEV jtag_uart
#define ALT_STDIN_PRESENT
#define ALT_STDOUT "/dev/jtag_uart"
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT_BASE 0x00901070
#define ALT_STDOUT_DEV jtag_uart
#define ALT_STDOUT_PRESENT
#define ALT_STDERR "/dev/jtag_uart"
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDERR_BASE 0x00901070
#define ALT_STDERR_DEV jtag_uart
#define ALT_STDERR_PRESENT
#define ALT_CPU_FREQ 50000000
#define ALT_IRQ_BASE NULL

/*
 * processor configuration
 *
 */

#define NIOS2_CPU_IMPLEMENTATION "small"
#define NIOS2_BIG_ENDIAN 0

#define NIOS2_ICACHE_SIZE 4096
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE 32
#define NIOS2_ICACHE_LINE_SIZE_LOG2 5
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_FLUSHDA_SUPPORTED

#define NIOS2_EXCEPTION_ADDR 0x00880020
#define NIOS2_RESET_ADDR 0x00880000
#define NIOS2_BREAK_ADDR 0x00900820

#define NIOS2_HAS_DEBUG_STUB

#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0

/*
 * A define for each class of peripheral
 *
 */

#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_UART
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_CFI_FLASH
#define __ALTERA_AVALON_SYSID
#define __ALTERA_AVALON_TRI_STATE_BRIDGE
#define __ALTERA_AVALON_TIMER
#define __SRAM_16BIT_512K
#define __ALTERA_AVALON_LCD_16207
#define __CCD_CONTROLLER

/*
 * jtag_uart configuration
 *
 */

#define JTAG_UART_NAME "/dev/jtag_uart"
#define JTAG_UART_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_BASE 0x00901070
#define JTAG_UART_SPAN 8
#define JTAG_UART_IRQ 0
#define JTAG_UART_WRITE_DEPTH 64
#define JTAG_UART_READ_DEPTH 64
#define JTAG_UART_WRITE_THRESHOLD 8
#define JTAG_UART_READ_THRESHOLD 8
#define JTAG_UART_READ_CHAR_STREAM ""
#define JTAG_UART_SHOWASCII 1
#define JTAG_UART_READ_LE 0
#define JTAG_UART_WRITE_LE 0
#define JTAG_UART_ALTERA_SHOW_UNRELEASED_JTAG_UART_FEATURES 0
#define ALT_MODULE_CLASS_jtag_uart altera_avalon_jtag_uart

/*
 * uart configuration
 *
 */

#define UART_NAME "/dev/uart"
#define UART_TYPE "altera_avalon_uart"
#define UART_BASE 0x00901000
#define UART_SPAN 32
#define UART_IRQ 1
#define UART_BAUD 115200
#define UART_DATA_BITS 8
#define UART_FIXED_BAUD 1
#define UART_PARITY 'N'
#define UART_STOP_BITS 1
#define UART_USE_CTS_RTS 0
#define UART_USE_EOP_REGISTER 0
#define UART_SIM_TRUE_BAUD 0
#define UART_SIM_CHAR_STREAM ""
#define UART_FREQ 50000000
#define ALT_MODULE_CLASS_uart altera_avalon_uart

/*
 * sw_pio configuration
 *
 */

#define SW_PIO_NAME "/dev/sw_pio"
#define SW_PIO_TYPE "altera_avalon_pio"
#define SW_PIO_BASE 0x00901040
#define SW_PIO_SPAN 16
#define SW_PIO_DO_TEST_BENCH_WIRING 0
#define SW_PIO_DRIVEN_SIM_VALUE 0
#define SW_PIO_HAS_TRI 0
#define SW_PIO_HAS_OUT 0
#define SW_PIO_HAS_IN 1
#define SW_PIO_CAPTURE 0
#define SW_PIO_DATA_WIDTH 18
#define SW_PIO_EDGE_TYPE "NONE"
#define SW_PIO_IRQ_TYPE "NONE"
#define SW_PIO_BIT_CLEARING_EDGE_REGISTER 0
#define SW_PIO_FREQ 50000000
#define ALT_MODULE_CLASS_sw_pio altera_avalon_pio

/*
 * key_pio configuration
 *
 */

#define KEY_PIO_NAME "/dev/key_pio"
#define KEY_PIO_TYPE "altera_avalon_pio"
#define KEY_PIO_BASE 0x00901050
#define KEY_PIO_SPAN 16
#define KEY_PIO_DO_TEST_BENCH_WIRING 0
#define KEY_PIO_DRIVEN_SIM_VALUE 0
#define KEY_PIO_HAS_TRI 0
#define KEY_PIO_HAS_OUT 0
#define KEY_PIO_HAS_IN 1
#define KEY_PIO_CAPTURE 0
#define KEY_PIO_DATA_WIDTH 3
#define KEY_PIO_EDGE_TYPE "NONE"
#define KEY_PIO_IRQ_TYPE "NONE"
#define KEY_PIO_BIT_CLEARING_EDGE_REGISTER 0
#define KEY_PIO_FREQ 50000000
#define ALT_MODULE_CLASS_key_pio altera_avalon_pio

/*
 * cfi_flash configuration
 *
 */

#define CFI_FLASH_NAME "/dev/cfi_flash"
#define CFI_FLASH_TYPE "altera_avalon_cfi_flash"
#define CFI_FLASH_BASE 0x00400000
#define CFI_FLASH_SPAN 4194304
#define CFI_FLASH_SETUP_VALUE 0
#define CFI_FLASH_WAIT_VALUE 0
#define CFI_FLASH_HOLD_VALUE 0
#define CFI_FLASH_TIMING_UNITS "ns"
#define CFI_FLASH_UNIT_MULTIPLIER 1
#define CFI_FLASH_SIZE 4194304
#define ALT_MODULE_CLASS_cfi_flash altera_avalon_cfi_flash

/*
 * sysid configuration
 *
 */

#define SYSID_NAME "/dev/sysid"
#define SYSID_TYPE "altera_avalon_sysid"
#define SYSID_BASE 0x00901078
#define SYSID_SPAN 8
#define SYSID_ID 1712698347u
#define SYSID_TIMESTAMP 1303991315u
#define SYSID_REGENERATE_VALUES 0
#define ALT_MODULE_CLASS_sysid altera_avalon_sysid

/*
 * tristate_bridge configuration
 *
 */

#define TRISTATE_BRIDGE_NAME "/dev/tristate_bridge"
#define TRISTATE_BRIDGE_TYPE "altera_avalon_tri_state_bridge"
#define ALT_MODULE_CLASS_tristate_bridge altera_avalon_tri_state_bridge

/*
 * timer configuration
 *
 */

#define TIMER_NAME "/dev/timer"
#define TIMER_TYPE "altera_avalon_timer"
#define TIMER_BASE 0x00901020
#define TIMER_SPAN 32
#define TIMER_IRQ 2
#define TIMER_ALWAYS_RUN 0
#define TIMER_FIXED_PERIOD 0
#define TIMER_SNAPSHOT 1
#define TIMER_PERIOD 1.0
#define TIMER_PERIOD_UNITS "ms"
#define TIMER_RESET_OUTPUT 0
#define TIMER_TIMEOUT_PULSE_OUTPUT 0
#define TIMER_LOAD_VALUE 49999
#define TIMER_MULT 0.001
#define TIMER_FREQ 50000000
#define ALT_MODULE_CLASS_timer altera_avalon_timer

/*
 * sram_16bit_512k_0 configuration
 *
 */

#define SRAM_16BIT_512K_0_NAME "/dev/sram_16bit_512k_0"
#define SRAM_16BIT_512K_0_TYPE "sram_16bit_512k"
#define SRAM_16BIT_512K_0_BASE 0x00880000
#define SRAM_16BIT_512K_0_SPAN 524288
#define SRAM_16BIT_512K_0_HDL_PARAMETERS ""
#define ALT_MODULE_CLASS_sram_16bit_512k_0 sram_16bit_512k

/*
 * lcd configuration
 *
 */

#define LCD_NAME "/dev/lcd"
#define LCD_TYPE "altera_avalon_lcd_16207"
#define LCD_BASE 0x00901060
#define LCD_SPAN 16
#define ALT_MODULE_CLASS_lcd altera_avalon_lcd_16207

/*
 * CCD_Controller_inst configuration
 *
 */

#define CCD_CONTROLLER_INST_NAME "/dev/CCD_Controller_inst"
#define CCD_CONTROLLER_INST_TYPE "CCD_Controller"
#define CCD_CONTROLLER_INST_BASE 0x00901080
#define CCD_CONTROLLER_INST_SPAN 8
#define ALT_MODULE_CLASS_CCD_Controller_inst CCD_Controller

/*
 * system library configuration
 *
 */

#define ALT_MAX_FD 32
#define ALT_SYS_CLK TIMER
#define ALT_TIMESTAMP_CLK none

/*
 * Devices associated with code sections.
 *
 */

#define ALT_TEXT_DEVICE       SRAM_16BIT_512K_0
#define ALT_RODATA_DEVICE     SRAM_16BIT_512K_0
#define ALT_RWDATA_DEVICE     SRAM_16BIT_512K_0
#define ALT_EXCEPTIONS_DEVICE SRAM_16BIT_512K_0
#define ALT_RESET_DEVICE      SRAM_16BIT_512K_0

/*
 * The text section is initialised so no bootloader will be required.
 * Set a variable to tell crt0.S to provide code at the reset address and
 * to initialise rwdata if appropriate.
 */

#define ALT_NO_BOOTLOADER


#endif /* __SYSTEM_H_ */
