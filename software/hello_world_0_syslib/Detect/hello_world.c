#include <stdio.h>
#include "CCD_Controller_API.h"

int main() {  
  while(1) {
    // read switch
    unsigned char key = read_pio_key(); 
    unsigned int  sw  = read_pio_sw(); 
    
    // write CCD
    write_ccd_sw(sw);
    write_ccd_key(key);
  }
}

