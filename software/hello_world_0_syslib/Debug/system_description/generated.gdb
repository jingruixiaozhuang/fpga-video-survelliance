# generated.gdb
#
# Machine generated for a CPU named "cpu" as defined in:
# d:\workplace\END_de2_detect\DE2_LCM_CCD_detect_b\software\hello_world_0_syslib\..\..\system_0.ptf
#
# Generated: 2011-04-29 10:04:38.5

# DO NOT MODIFY THIS FILE
#
#   Changing this file will have subtle consequences
#   which will almost certainly lead to a nonfunctioning
#   system. If you do modify this file, be aware that your
#   changes will be overwritten and lost when this file
#   is generated again.
#
# DO NOT MODIFY THIS FILE

# cfi_flash
mem 0x00400000 0x00400000+4194304 cache

# sram_16bit_512k_0
mem 0x00880000 0x00880000+524288 cache
