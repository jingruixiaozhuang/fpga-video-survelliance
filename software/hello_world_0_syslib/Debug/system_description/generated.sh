#!/bin/sh
#
# generated.sh - shell script fragment - not very useful on its own
#
# Machine generated for a CPU named "cpu" as defined in:
# d:\workplace\END_de2_detect\DE2_LCM_CCD_detect_b\software\hello_world_0_syslib\..\..\system_0.ptf
#
# Generated: 2011-04-29 10:04:36.843

# DO NOT MODIFY THIS FILE
#
#   Changing this file will have subtle consequences
#   which will almost certainly lead to a nonfunctioning
#   system. If you do modify this file, be aware that your
#   changes will be overwritten and lost when this file
#   is generated again.
#
# DO NOT MODIFY THIS FILE

# This variable indicates where the PTF file for this design is located
ptf=d:\workplace\END_de2_detect\DE2_LCM_CCD_detect_b\software\hello_world_0_syslib\..\..\system_0.ptf

# This variable indicates whether there is a CPU debug core
nios2_debug_core=yes

# This variable indicates how to connect to the CPU debug core
nios2_instance=0

# This variable indicates the CPU module name
nios2_cpu_name=cpu

# These variables indicate what the System ID peripheral should hold
sidp=0x00901078
id=1712698347u
timestamp=1303991315u

# Include operating system specific parameters, if they are supplied.

if test -f /cygdrive/d/PROGRA~1/altera/nios2eds/nios2eds/components/altera_hal/build/os.sh ; then
   . /cygdrive/d/PROGRA~1/altera/nios2eds/nios2eds/components/altera_hal/build/os.sh
fi
